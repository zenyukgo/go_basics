package abc

type Status int

const (
	Up Status = iota
	Down
	Error
)
