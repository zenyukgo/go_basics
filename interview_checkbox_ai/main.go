package main

import "fmt"

/*

Write a function that returns the minimum number of legal tender (in Australian notes and coins) required to make up a certain amount of money. The amount of money will be an integer representing the total number of cents.

Example 1
If the input was 2250, the result should be 3.
This is because 2250 cents represents $22.50, and would be broken down into 3 pieces of legal tender ($20 note, $2 coin, 50c coin)

Example 2
If the input is 1125, the result should be 4.
$11.25 consists of a $10 note, $1 coin, 20c coin & 5c coin

*/

func main() {
	result1 := countTenders(2)
	result2 := countTenders(2250)
	result3 := countTenders(1125)
	result4 := countTenders(4050)
	result5 := countTenders(4455)

	fmt.Printf("2: %v\n 2250: %v \n 1125: %v \n 4050: %v \n 4455: %v \n", result1, result2, result3, result4, result5)
}

func countTenders(amount int) (tendersCount int) {
	count := 0
	amountLeft := amount
	count, amountLeft = countForTender(amountLeft, 10000)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 5000)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 2000)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 1000)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 500)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 200)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 100)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 50)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 20)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 10)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 5)
	tendersCount += count

	count, amountLeft = countForTender(amountLeft, 1)
	tendersCount += count

	return
}

func countForTender(amount int, tender int) (count int, left int) {
	left = amount
	for left - tender >= 0 {
		left = left - tender
		count++
	}
	return
}


/*

100 dollars - 10_000 cents
50 dollars -> 5_000 cents
20 dollars -> 2_000 cents
10 dollars -> 1_000
5 dollars	->  500
1 dollar  ->  100
50 cents
20 cents
10 cents
5 cents
1 cent

*/