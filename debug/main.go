package main

import (
	"fmt"
	"time"
)

func main() {
	i := 1
	b := i + 2
	for {
		b += 2
		c := i + b
		fmt.Printf("result: %d\n", c)
		time.Sleep(1 * time.Second)
	}
}
/*
How to attach with "dlv"

- go get delve
- build the debug binary
- copy the binary (assume it's called "debug") to ~
- run it
- cd to any other folder
- pgrep debug, copy PID
- run "dlv attach YOUR_PID", process will be paused
- (dlv) goroutines - prints all goroutes
- (dlv) goroutine 3 - switch to a goroutine e.g. 3
- (dlv) next - debug to next line
- (dlv) list - print current code
- (dlv) print b - print value of variable "b"
- (dlv) break 12 - set break point on line 12
- (dlv) breakpoints - list all breakpoints
- (dlv) continue - runs until breakpoint
- (dlv) q - exit, asking to stop process
*/
