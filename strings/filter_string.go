//amazon 2018
package main

var (
	restricted = []string{"a", "an", "the", "at", "this", "is"}
	splitters  = []string{" ", ".", ","}
)

func main() {
	phrase := "this is a nice guy at the corner is nice"
	for _, keyword := range solve(phrase) {
		println(keyword)
	}
}

func solve(phrase string) (keywords []string) {
	phrase += " "
	word := ""
	for _, s := range phrase {
		symbol := string(s)
		if isSplitterSymbol(symbol) {
			if len(word) > 0 &&
				!alreadyContainsWord(word, keywords) &&
				!isRestrictedWord(word) {

				keywords = append(keywords, word)
			}
			word = ""
			continue
		}
		word += symbol
	}
	return
}

func isRestrictedWord(word string) bool {
	for _, restricted := range restricted {
		if word == restricted {
			return true
		}
	}
	return false
}

func alreadyContainsWord(word string, keywords []string) bool {
	for _, keyword := range keywords {
		if word == keyword {
			return true
		}
	}
	return false
}

func isSplitterSymbol(symbol string) bool {
	for _, splitter := range splitters {
		if symbol == splitter {
			return true
		}
	}
	return false
}

