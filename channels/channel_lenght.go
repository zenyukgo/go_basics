package main

import "fmt"

func main() {
	c := make(chan struct{}, 10)
	c <- struct{}{}
	c <- struct{}{}
	c <- struct{}{}
	c <- struct{}{}
	c <- struct{}{}
	<-c
	l := len(c)
	fmt.Printf("length: %+v\n", l)
}
