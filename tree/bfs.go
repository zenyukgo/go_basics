/**
 binary tree traversal
 Breadth first search BFS

      a
    b   c
   d e f g

output:
abcdefg

*/

package main

import (
	"container/list"
	"fmt"
)

type Node struct {
	value string
	left  *Node
	right *Node
}

func main() {
	stack := list.New()
	stack.Init()
	tree := generateTree()
	stack.PushFront(tree)
	for stack.Len() > 0 {
		stackItem := stack.Front()
		node, ok := stackItem.Value.(*Node)
		if !ok {
			break
		}
		fmt.Printf("element: %v\n", node.value)
		if node.left != nil {
			stack.PushBack(node.left)
		}
		if node.right != nil {
			stack.PushBack(node.right)
		}
		stack.Remove(stackItem)
	}
}

func generateTree() *Node {
	g := &Node{value: "g"}
	f := &Node{value: "f"}
	e := &Node{value: "e"}
	d := &Node{value: "d"}
	c := &Node{
		value: "c",
		left:  f,
		right: g,
	}
	b := &Node{
		value: "b",
		left:  d,
		right: e,
	}
	return &Node{
		value: "a",
		left:  b,
		right: c,
	}
}
