package main

import (
	"fmt"
)

func main() {
	var big uint64 = 533
	var small int8 = 5
	var result int = int(big - uint64(small))
	fmt.Printf("result: %v \n", result)

	var f32 float32 = 16777216.0
	var floatResultA float32 = f32 + 1
	var floatResultB float32 = floatResultA + 1
	var floatResultC float32 = floatResultB + 1
	fmt.Printf("float result a: %v \n", floatResultA)
	fmt.Printf("float result a: %v \n", floatResultB)
	fmt.Printf("float result a: %v \n", floatResultC)
}
