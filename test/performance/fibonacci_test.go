package performance

import (
	"testing"
)

func TestFibonacciSimple(t *testing.T) {
	result := fibonacciSimple(10)
	if result != 55 {
		t.Errorf("expecting 55, but got %d", result)
	}
}

func TestFibonacciChannels(t *testing.T) {
	result := fibonacciChannels(10)
	if result != 55 {
		t.Errorf("expecting 55, but got %d", result)
	}
}

func BenchmarkFibonacciSimple(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacciSimple(i)
	}
}

func BenchmarkFibonacciChannels(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacciChannels(i)
	}
}

/* Output:

▶ go test -bench .

BenchmarkFibonacciSimple-6     	  344498	     43763 ns/op
BenchmarkFibonacciChannels-6   	   10000	   6069595 ns/op
PASS
ok  	go_basics/test/performance	75.820s

*/