package main

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"unicode"
)

/*	Rename all files in a given directory
	Original:
		lesson1.mp4 .. lesson10.mp4
	Renamed:
		001.mp4 .. 010.mp4
	Could be used to reformat files for VLC video player. To play all files in order
 */
func main() {
	dir := os.Args[1]
	files, err := ioutil.ReadDir(dir)
	if err != nil || len(files) < 1{
		log.Fatal(err)
	}
	beginningWord, ok := parseBeginningWord(files[0].Name())
	if !ok {
		log.Fatal(errors.New("No beginning suffix found. "))
	}
	suffixLen := len(beginningWord)
	var newName string
	for _, file := range files {
		newName = newFilename(file.Name(), suffixLen)
		oldFilePath := dir + "/" + file.Name()
		newFilePath := dir + "/" + newName
		err := os.Rename(oldFilePath, newFilePath)
		if err != nil {
			println("Can't rename file: " + oldFilePath)
		}
	}

}

/*
	Input like	: lesson2.mp4
	Output 		: lesson
*/
func parseBeginningWord(name string) (result string, ok bool) {
	numbersStartsAt := -1
	for i, r := range name {
		if unicode.IsDigit(r) {
			numbersStartsAt = i
			break
		}
	}
	if numbersStartsAt < 1 {
		return "", false
	}
	result = name[:numbersStartsAt]
	return result, true
}

/*
	Input like	: lesson2.mp4
	Output 		: 002.mp4
*/
func newFilename(original string, beginningSuffixLen int) (result string) {
	endingTemplate := ".mp4"
	digitPositionsCount := len(original) - len(endingTemplate) - beginningSuffixLen
	if digitPositionsCount < 1 {
		log.Fatal(errors.New("Can't rename file, error counting digital positions. "))
	}
	var nameBeforeDot string
	switch digitPositionsCount {
	case 1:
		nameBeforeDot = "00" + original[beginningSuffixLen:beginningSuffixLen + 1]
	case 2:
		nameBeforeDot = "0" + original[beginningSuffixLen:beginningSuffixLen + 2]
	case 3:
		nameBeforeDot = original[beginningSuffixLen:beginningSuffixLen + 3]
	}
	result = nameBeforeDot + endingTemplate
	return
}