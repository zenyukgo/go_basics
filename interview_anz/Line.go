package main

import (
	"sort"
)

type Line [2]int

func getInput() []Line {
	/*
	   customer Id.  account Id
	   1             101
	   2             103
	   3             107
	   2             101
	   1             103
	   8             105
	   9             107
	   11            101
	   11            103
	*/
	return []Line{{1, 101}, {2, 103}, {3, 107}, {2, 101}, {1, 103}, {8, 105}, {9, 107}, {11, 101}, {11, 103}}
}


// link returns two associations: customers grouped by account and accounts grouped by customer
// map values are ordered slices
func link(input []Line) (map[int]Customers, map[int]Accounts) {
	accounts := map[int]Customers{}
	customers := map[int]Accounts{}
	for _, l := range input {
		customerId := l[0]
		accountId := l[1]

		_, aOk := accounts[accountId]
		if aOk {
			accounts[accountId] = append(accounts[accountId], customerId)
			sort.Ints(accounts[accountId])
		} else {
			accounts[accountId] = []int{customerId}
		}

		_, cOk := customers[customerId]
		if cOk {
			customers[customerId] = append(customers[customerId], accountId)
			sort.Ints(customers[customerId])
		} else {
			customers[customerId] = []int{accountId}
		}
	}
	return accounts, customers
}