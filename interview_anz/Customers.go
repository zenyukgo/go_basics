package main

import (
	"sort"
	"strconv"
	"strings"
)

type Customers []int

func (c Customers) sameAs(other Customers) bool {
	b := strings.Builder{}
	for _, id := range c {
		b.WriteString(strconv.Itoa(id))
	}
	s1 := b.String()
	b.Reset()
	for _, id := range other {
		b.WriteString(strconv.Itoa(id))
	}
	s2 := b.String()
	return s1 == s2
}

func (c Customers) hasMatchingCustomerGroup(all []Customers) bool {
	for _, g := range all {
		if g.sameAs(c) {
			return true
		}
	}
	return false
}

func getAllMatchingCustomersWithSameAccounts(accountsByCustomer map[int]Accounts, customer int) Customers {
	result := Customers{customer}
	theCustomerAccounts := accountsByCustomer[customer]
	for c, accounts := range accountsByCustomer {
		if c == customer {
			continue
		}
		if accounts.sameAs(theCustomerAccounts) {
			result = append(result, c)
			sort.Ints(result)
		}
	}
	return result
}