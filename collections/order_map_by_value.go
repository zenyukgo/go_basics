package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Ordering map[string]int by value")

	m := map[string]int{}
	m["igor"] = 36
	m["victoria"] = 11
	m["nadia"] = 31

	// handmade
	s := orderMapByValue(m)
	fmt.Printf("%v\n", s)

	// std
	s = usingStdSort(m)
	fmt.Printf("%v\n", s)
}

func orderMapByValue(m map[string]int) (orderedKeys []string) {
	for name := range m {
		orderedKeys = append(orderedKeys, name)
	}
	var temp string
	for i := 0; i < len(orderedKeys); i++ {
		for j := 0; j < len(orderedKeys); j++ {
			if m[orderedKeys[i]] < m[orderedKeys[j]] {
				temp = orderedKeys[i]
				orderedKeys[i] = orderedKeys[j]
				orderedKeys[j] = temp
			}
		}
	}
	return
}

func usingStdSort(m map[string]int) (orderedKeys []string) {
	for name := range m {
		orderedKeys = append(orderedKeys, name)
	}
	sort.Slice(orderedKeys, func(i, j int) bool { return m[orderedKeys[i]] < m[orderedKeys[j]] })
	return
}
