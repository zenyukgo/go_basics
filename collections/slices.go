package main

import (
	"fmt"
	"reflect"
)

// Exists checks if a slice of any type contains an element and return index
func Exists(slice interface{}, item interface{}) (bool, int, error) {
	v := reflect.ValueOf(slice)
	if v.Kind() != reflect.Slice {
		return false, -1, fmt.Errorf("method 'Exists' is designed for a slice and can not operate on %v", v.Kind().String())
	}
	for i := 0; i < v.Len(); i++ {
		if v.Index(i).Interface() == item {
			return true, i, nil
		}
	}
	return false, -1, nil
}

// convert array to slice
// mySlice := myArray[:]

func main() {
	s := make([]bool, 22)
	s[21] = true
	s[13] = true
	s[15] = false

	fmt.Printf("slice: %v\n", s)
	// slice: [false false false false false false false false false false false false false true false false false false false false false true]
}